<?php

// Name: 		Download Button
// Version: 	1.0
// Author: 		OpenCart FACTORY
// Website:		www.ocfactory.net

class ControllerProductDownload extends Controller {
	public function index() {
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		if ($product_id == 0) {
			exit;
		}

		$query = $this->db->query("SELECT d.filename, d.mask, d.download_id FROM " . DB_PREFIX . "download d LEFT JOIN " . DB_PREFIX . "product_to_download p2d ON (p2d.product_id = '" . $product_id . "') WHERE d.download_id = p2d.download_id");

		if(isset($query->row)) {
			$file = DIR_DOWNLOAD . $query->row['filename'];
			$mask = basename($query->row['mask']);


			if (!headers_sent()) {
				if (file_exists($file)) {
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));

					if (ob_get_level()) {
						ob_end_clean();
					}

					readfile($file, 'rb');
					exit();

				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
		}
	}
}